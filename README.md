# udiskie-user-umount

A simple unmounting script for udiskie users.

## Dependencies

- coreutils
- udiskie (udiskie-umount)
- libnotify (notify-send)
- zenity

## Usage

The script only takes one optional argument:

```txt
udiskie-user-umount [-h] <mounts root>
    -h          : print the usage message
    mounts_root : the root directory for user mounts (default: /run/media/$USER)
```

A typical use case for such a script would be as a helper bound to a keyboard
combination. For example, with Openbox:

```xml
<keybind key="W-KP_Subtract">
    <action name="Execute">
        <command>udiskie-user-umount</command>
    </action>
</keybind>
```

When no mount point is found, the script simply exits with a notification. If only one mount point is available, it is immediately unmounted. When several mount points are found, the script uses zenity to prompt the user. Devices are automatically detached when their last mount point is unmounted.
